// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// -- COMMENT THIS prior to pushing to "hereheh"
// =============================================
/**/ 
if( process.env.NODE_ENV !== 'production' ){ require('dotenv').config(); } 
/**/

// =============================================
// process.env SETUP
// =============================================
const { NODE_ENV } = process.env;
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production'; // true
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development'; // true

const express = require('express');
const routes = require('./routes');
const http = require('node:http');
const path = require('node:path');
const hbs = require('hbs');

const app = express();
let router = express.Router();

// =============================================
// Establish the NODE_ENV toggle settings
// ==============================================

// app.set('port', process.env.PORT || 3000 );
app.set( 'port' , process.env.PORT );
app.set( 'views' , path.join( __dirname, 'views' ) );
app.set( 'view engine' , 'html' );
app.engine( 'html', hbs.__express );

app.use( express.json() );
app.use( express.urlencoded( { extended: true } ) );
app.use( router );

app.use( express.static( path.join( __dirname, 'public' ) ) );

app.get( '/' , routes.index );
app.get( '/cover' , routes.cover );
app.get( '/cache' , routes.cache );

http.createServer( app ).listen( app.get('port') , function(){

  // console.log('Running at http://localhost:' + app.get('port') );
  // console.log('Running at port' + app.get('port') ); 

  if( IS_PRODUCTION_NODE_ENV === true ){
    console.log('Take one...');
  }
  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log('Running at http://localhost:' + app.get('port') );
  }

} );

// BASED ON ... https://www.raymondcamden.com/2014/02/02/Examples-of-the-Marvel-API