( () => {
  
  const marvel = require('../marvel');

  exports.index = (req,res,next) => {
    res.render( 'index' , { title: 'Marvel Comic Cover Viewer ' } );
  };

  exports.cover = (req,res,next) => {
    let cover = marvel.getCover( (cover) => {
      res.writeHead( 200, { 'Content-Type' : 'application/json' } );
      res.write( JSON.stringify(cover) );
      res.end();
    } );

    // return cover;
  };

  exports.cache = (req,res,next) => {
    
    let cache = marvel.getCache();
    let cacheDisplay = [];
    
    for( let c in cache ){
      cacheDisplay.push( {
        key: c
        , hits: cache[c].hits
        , images: cache[c].images.length
      } );
    }

    // console.log( "cacheDisplay", cacheDisplay );

    res.render( 'cache' , { cache: cacheDisplay } );
  };

} )();