( () => {
  
  let img1 = document.querySelector('#image-1');
  let	sourceText = document.querySelector('#attrib-copy');
  let ms = 8437; // 15000 11250 5625 8437

  const formatDate = (d) => {
    return ( ( d.getMonth() + 1 ) + "/" + d.getFullYear() );
  };

  const getCover = () => {
    
    const myUrl = new Request( '/cover' );

    return fetch( myUrl )
    .then( ( response ) => {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response); // return response;
      } 
      else {
        let error = new Error( response.statusText );
        error.response = response;
        return Promise.reject( error ); // throw error;
      }
    } )
    .then( ( response ) => {
      return response.json();
    } )
    .then( ( data ) => {
      // console.log( "data.url" , data.url );
      img1.setAttribute( "src" , data.url );
      let d = new Date(data.date);
      let source = data.title + " / Published " + formatDate(d);
      sourceText.innerHTML = "";
      sourceText.insertAdjacentText( "beforeend" , source );
      setTimeout( getCover, ms );
    } )
    .catch( (err) => {
      
      // console.log( "err", err );

    } );

  };

  // every N seconds i ask for a new image i... add the image to the queue

  const initAll = () => {
    getCover();
    sourceText.insertAdjacentHTML( "beforeend" , "<i>Loading...</i>" );
  };

  document.addEventListener( "DOMContentLoaded", initAll , false );
} )();