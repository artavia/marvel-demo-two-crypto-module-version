( () => {
  
  // =============================================
  // LOCAL DEVELOPMENT ENV VARS SETUP
  // -- COMMENT THIS prior to pushing to "hereheh"
  // =============================================
  /**/ 
  if( process.env.NODE_ENV !== 'production' ){ require('dotenv').config(); } 
  /**/

  const https = require('node:https');
  // const crypto = require('node:crypto'); // OLD 2019
  const { createHash } = require('node:crypto'); // NEW 2023
  

  let cache = [];

  const { PRIV_KEY, API_KEY } = process.env;

  const IMAGE_NOT_AVAIL = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available";

  exports.getCache = () => {
    return cache;
  };

  const getRandomInt = (min,max) => {
    return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
  };

  Object.size = (obj) => {
    let size = 0; 
    for( let key in obj ){
      if( obj.hasOwnProperty(key) ){
        size++;
      }
    }
    return size;
  };

  const getCover = (cb) => {
    // select a random year and random month
    let start = 1988; // let start = 2013;
    let end = 1986; // let end = 2011;

    let year = getRandomInt(start,end);
    let month = getRandomInt(1,12);

    let cache_key = `${year}_${month}`;

    if( cache_key in cache ){

      console.log( 'Had cache for ' , cache_key );

      let images = cache[cache_key].images;
      cache[cache_key].hits++;
      cb( images[ getRandomInt( 0, images.length-1 ) ] );
    }
    else{
      
      let monthStr = month < 10 ? `0${month}` : month;

      let eom = month === 2 ? 28 : (month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12 ) ? 31 : 30;

      let beginDateStr = `${year}-${monthStr}-01`;
      let endDateStr = `${year}-${monthStr}-${eom}`;

      let TS = new Date().getTime();
      let stringToHash = `${TS}${PRIV_KEY}${API_KEY}`;
      
      // let ALPHAHASH = crypto.createHash("md5").update(stringToHash).digest('hex'); // OLD 2019
      let ALPHAHASH = createHash("md5").update(stringToHash).digest('hex'); // NEW 2023

      // https://developer.marvel.com/documentation/authorization
      // Authentication for Server-Side Applications
      /*
      Server-side applications must pass two parameters in addition to the apikey parameter:

        ts - a timestamp (or other long string which can change on a request-by-request basis)
        hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)

      For example, a user with a public key of "1234" and a private key of "abcd" could construct a valid call as follows: http://gateway.marvel.com/v1/public/comics?ts=1&apikey=1234&hash=ffd275c5130566a2916217b101f26150 (the hash value is the md5 digest of 1abcd1234)
      */ 

      // console.log("TS", TS);
      // console.log("stringToHash", stringToHash);
      // console.log("ALPHAHASH", ALPHAHASH);

      let baseUrl = 'https://gateway.marvel.com:443/v1/public';
      let path = '/comics';
      let requiredparams = `?apikey=${API_KEY}&ts=${TS}&hash=${ALPHAHASH}`; 
      let optionalparams = `&limit=100&format=comic&formatType=comic&dateRange=${beginDateStr}%2C${endDateStr}`; 

      let url = `${baseUrl}${path}${requiredparams}${optionalparams}`; 
      // console.log("url", url);

      // console.log( new Date() + ' ' + url );

      https.get( url , (res) => {
        
        let body = "";

        res.on( 'data', (chunk) => {
          body += chunk;
        } );

        res.on( 'end', () => {
          
          let result = JSON.parse(body);
          console.log( "result", result );
          
          let images;
          
          if( result.code === 200 ){
            
            images = [];

            console.log( `Number of comics: ${ result.data.results.length }` );

            // start sammich
            for( let i=0; i<result.data.results.length; i++){
              
              let comic = result.data.results[i];
              console.dir( "comic" , comic );

              if( (comic.thumbnail && comic.thumbnail.path !== IMAGE_NOT_AVAIL) && ( comic.title !== undefined ) ){
                let image = {};
                image.title = comic.title;
                
                for( let x=0; x<comic.dates.length; x++){
                  if(comic.dates[x].type === 'onsaleDate'){
                    image.date = new Date( comic.dates[x].date );
                  }
                }

                image.url = `${comic.thumbnail.path}.${comic.thumbnail.extension}`;
                
                images.push(image);
              }
            }
            // END sammich

            console.dir( "images" , images );
            
            cache[cache_key] = { hits: 1 };
            cache[cache_key].images = images;
            cb( images[ getRandomInt(0, images.length-1 ) ] );
          }
          else
          if( result.code === "RequestThrottled "){
            
            console.log("RequestThrottled Error");
            
            // Don't just fail. If there is a cache grab it 
            if( Object.size( cache ) > 5 ){
              let keys = [];
              for( let k in cache ){
                keys.push( k );
              }

              let randomCacheKey = keys[ getRandomInt( 0, keys.length-1 ) ];
              images = cache[randomCacheKey].images;
              cache[randomCacheKey].hits++;
              cb( images[ getRandomInt( 0, images.length-1 ) ] );
            }
            else{
              cb( { error: result.code } );
            }
          }
          else {

            console.log( ` ${ new Date() } Error: ${JSON.stringify(result) }` );
            
            cb( { error: result.code } );
          }

        } );

      } );

      
    }

  };

  exports.getCover = getCover;

} )();