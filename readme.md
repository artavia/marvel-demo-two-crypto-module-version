# Marvel Demo Two

## Description
Marvel demo two originally by Ray Camden&hellip; re&#45;expressed by yours truly.

## Addendum Number One with Justification
This project has been updated for use subsequent to December 2022. I had five (5) projects simultaneously go to the pot given that Heroku brought Stack 18 to [EOL as explained here](https://help.heroku.com/X5OE6BCA/heroku-18-end-of-life-faq "link to Heroku Help"), hence, a slight re-working of said projects (this is no exception).

## Updated status (2023) ~~Please visit~~
Sorry but subsequent to December 2022 and as a result of the new policy at SalesForce/Heroku, free plans are no longer available to the general public. Thus, the site will be left in an inoperable state. ~~You can see [the lesson at Heroku](https://marvel-cryptomodule-demo.herokuapp.com/ "the lesson at heroku") and decide if this is something for you to play around with at a later date.~~

## Final Addendum (2023)
This project utilized Heroku. And it will be currently mothballed as a result of *limited resources*. The five affected projects in question are:
  - [marvel-demo-two-crypto-module-version](https://gitlab.com/artavia/marvel-demo-two-crypto-module-version "link to Gitlab Repository");
  - [marvel-demo-three-webpack-version](https://gitlab.com/artavia/marvel-demo-three-webpack-version "link to Gitlab Repository");
  - [issuetracker-mean-demo](https://gitlab.com/artavia/issuetracker-mean-demo "link to Gitlab Repository");
  - [todotracker-mern-demo](https://gitlab.com/artavia/todotracker-mern-demo "link to Gitlab Repository");
  - [nacho-ordinary-mehn-example](https://gitlab.com/artavia/nacho-ordinary-mehn-example "link to Gitlab Repository");

## Re&#45;expressed? 
&quot;Dafuq izzat&quot; you probably asked. Hmmmm&hellip; I **loathe** JQuery, thus, out goes the baby with the bath-water! In terms of the native **http** module as it corresponds to nodejs I chose to use the **https** module instead. 

## For use with ~~Netlify~~ Heroku
This presentation is of the &quot;manual deployment&quot; in conjunction with drag&#45;drop for changes to take effect. 

To make a long story short, **I can just as easily use the CLI or drag and drop this prezzo**. 

In essence, today I decided that I [would] **use the CLI** and confirm the changes in the [~~Netlify~~ Heroku dashboard](https://dashboard.heroku.com/ "link to heroku dashboard"). **How do you like them apples?!**

## &quot;86&quot; any previous plans to use Netlify
I find less pushback in utilizing ~~Netlify~~ Heroku. Lord permitting, I will convert the project on another occasion so I may yet use Netlify. 

This setback should silence and humble any of you **code poachers** who are too eager to **love the tree and hate the fruit** or **love the fruit and hate the tree**. 

You, who are so arrogant and revel in plotting spite against your neighbor, because you are so smart **you can figure it out** on your own. 

Bye, Felicia!!! 

## Relevant experience
For me this article is what got the ball rolling in terms of JAM stack as explained by [Ray Camden](https://www.raymondcamden.com/2014/02/02/Examples-of-the-Marvel-API "link to Ray Camden&rsquo;s blog"). 

  - I got an opportunity to play with the [crypto module](https://nodejs.org/api/crypto.html "link to crypto documentation"). 
  - The Marvel Comics api is just a toy but it has a couple of valuable lessons contained underneath the hood. **Communication** to the api is **impossible** without some form of **cryptographical obfuscation** during the initial call to said api.
  - Last, it gives me another reason to use ~~Netlify~~ Heroku.

## My thoughts on Marvel comics, et. al.
I am not endorsing Marvel or any of the witchcraft and nefarious works of death that go along with it. 

I used to collect comics when I was a teenager growing up during the 80s (when I was lost and knew no better). If you are into Marvel or DC or any other of that **&quot;harmless artistic fantasy&quot;** in the year 2020, then, you should be worried because the truth is that it is evil, it is dirty and it is antichrist. 

**&quot;Marvel Not&quot;** and do the following if you are not convinced yet:
  - **Know your enemy** and view [this presentation in English on Youtube](https://www.youtube.com/watch?v=qbCbyf16iYc "Marvel not in English") (authored done by Little Light Studios) because it was eye&#45;opening to say the least and it reinforces a lot of what I initially suspected about these so-called &quot;brands&quot;;
  - or, you can visit this subsequent [presentation in Spanish on Youtube](https://www.youtube.com/watch?v=leRAsF7NmNo "Marvel not in Spanish") (authored by Dilo Al Mundo) in order to get an eyefull. Nobody is trying to fool you: **you only have to look at the comments** to gain a perspective;

And, if you think true Christianity is a joke and something to be scoffed at then you can go on and pursue your commerce, way of life, unrighteousness, wickedness and disobedience&#45;&#45; you are still **dead wrong** in the end!!! If you like worshipping false idols, then, you can enjoy a cup full of God&rsquo;s wrath, as well, cuz **you are just a bunch of suckers**! 

To the rest of you [feckless baal worshipers who like to worship ~~other idols~~ la negrita](https://www.youtube.com/watch?v=7PhlOsxfM_Y "the baal worshipers of today"), I would like to rhetorically ask you all a question because you have no shame and I want you to indulge me for a moment: **&hellip;are you ready for what comes next?**

I challenge any of you non-christians and biblical &quot;dogs&quot; to call on your precious so-called **Avengers**, or invoke your **psychology** while you are at it during **your** most dangerous hour. Go on! Say you&rsquo;re scared! I dare you! 

As for me and my household, we will continue to call on the precious blood of Jesus for protection!

Therefore, to [all] the fools who like the parrot the line in March 2020 that **we are all in this Coronavirus crisis together**, I call your bluff and rebuke you in the name of the Lord Jesus Christ. 

Meanwhile, you can enjoy **your** plagues and **your** pestilences because I will tell you that the **Lord Jesus Christ** who is the **incorruptible God** is my family's protection. 

I will stay protected in the cleft of the rock in He who is the Son of God; in He that is my safety and protection while my feet are planted on solid ground by trusting the Lord Jesus Christ as his indignation passes over my home. 

